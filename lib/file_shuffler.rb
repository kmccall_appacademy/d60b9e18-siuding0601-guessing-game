# Write file_shuffler program that: * prompts the user for a file name * reads that file
# * shuffles the lines * saves it to the file "{input_name}-shuffled.txt".

def file_shuffler
  puts "What file do you want to shuffle?"
  file_name = gets.chomp
  contents = File.readlines(file_name)
  puts contents.shuffle
  new_content =contents.shuffle
  f = File.open("#{file_name}-shuffled", "w")
  f.puts new_content
  f.close
end
